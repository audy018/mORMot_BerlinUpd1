object MainForm: TMainForm
  Left = 0
  Top = 0
  ClientHeight = 299
  ClientWidth = 635
  Caption = 'MainForm'
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 13
  object UniHTMLFrame1: TUniHTMLFrame
    Left = 0
    Top = 0
    Width = 635
    Height = 299
    Hint = ''
    HTML.Strings = (
      ''
      #9#9'<div class="container demo-1">'#9
      #9#9#9'<!-- Codrops top bar -->'
      ''
      ''
      #9#9#9'<div class="main clearfix">'
      #9#9#9#9'<div class="column">'
      
        #9#9#9#9#9'<p>The submenu of this menu will slide and fade in from the' +
        ' right while the previous level rotates slightly and disappears ' +
        'in the back.</p>'
      #9#9#9#9'</div>'
      #9#9#9#9'<div class="column">'
      #9#9#9#9#9'<div id="dl-menu" class="dl-menuwrapper">'
      #9#9#9#9#9#9'<button class="dl-trigger">Open Menu</button>'
      #9#9#9#9#9#9'<ul class="dl-menu">'
      #9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9'<a href="#">Fashion</a>'
      #9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9#9#9'<a href="#">Men</a>'
      #9#9#9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Shirts</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Jackets</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Chinos &amp; Trousers</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Jeans</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">T-Shirts</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Underwear</a></li>'
      #9#9#9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9#9#9'<a href="#">Women</a>'
      #9#9#9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Jackets</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Knits</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Jeans</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Dresses</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Blouses</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">T-Shirts</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Underwear</a></li>'
      #9#9#9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9#9#9'<a href="#">Children</a>'
      #9#9#9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Boys</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Girls</a></li>'
      #9#9#9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9'<a href="#">Electronics</a>'
      #9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Camera &amp; Photo</a></li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">TV &amp; Home Cinema</a></li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Phones</a></li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">PC &amp; Video Games</a></li>'
      #9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9'<a href="#">Furniture</a>'
      #9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9#9#9'<a href="#">Living Room</a>'
      #9#9#9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Sofas &amp; Loveseats</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Coffee &amp; Accent Tables</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Chairs &amp; Recliners</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Bookshelves</a></li>'
      #9#9#9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9#9#9'<a href="#">Bedroom</a>'
      #9#9#9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9#9#9#9#9'<a href="#">Beds</a>'
      #9#9#9#9#9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Upholstered Beds</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Divans</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Metal Beds</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Storage Beds</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Wooden Beds</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Children'#39's Beds</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Bedroom Sets</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Chests &amp; Dressers</a></li>'
      #9#9#9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Home Office</a></li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Dining &amp; Bar</a></li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Patio</a></li>'
      #9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9'<a href="#">Jewelry &amp; Watches</a>'
      #9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Fine Jewelry</a></li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Fashion Jewelry</a></li>'
      #9#9#9#9#9#9#9#9#9'<li><a href="#">Watches</a></li>'
      #9#9#9#9#9#9#9#9#9'<li>'
      #9#9#9#9#9#9#9#9#9#9'<a href="#">Wedding Jewelry</a>'
      #9#9#9#9#9#9#9#9#9#9'<ul class="dl-submenu">'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Engagement Rings</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Bridal Sets</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Women'#39's Wedding Bands</a></li>'
      #9#9#9#9#9#9#9#9#9#9#9'<li><a href="#">Men'#39's Wedding Bands</a></li>'
      #9#9#9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9#9#9'</ul>'
      #9#9#9#9#9#9#9'</li>'
      #9#9#9#9#9#9'</ul>'
      #9#9#9#9#9'</div><!-- /dl-menuwrapper -->'
      #9#9#9#9'</div>'
      #9#9#9'</div>'
      #9#9'</div><!-- /container -->'
      #9#9'<script>'
      #9#9#9'$(function() {'
      #9#9#9#9'$( '#39'#dl-menu'#39' ).dlmenu();'
      #9#9#9'});'
      #9#9'</script>')
    Align = alClient
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitLeft = 200
    ExplicitTop = 104
    ExplicitWidth = 256
    ExplicitHeight = 128
    object UniButton1: TUniButton
      Left = 64
      Top = 56
      Width = 75
      Height = 25
      Hint = ''
      Caption = 'UniButton1'
      TabOrder = 1
    end
  end
end
